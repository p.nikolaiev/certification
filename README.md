Where and how can you use JQuery with the SDK?
What is the main difference between Cheerio and JQuery?
When would you use CheerioCrawler and what are its limitations?
What are the main classes for managing requests and when and why would you use one instead of another?
How can you extract data from a page in Puppeteer without using JQuery?
What is the default concurrency/parallelism the SDK uses?

1. We can use JQuery with Apify SDK for manipulating the DOM of a HTML page.

2. JQuery used for manipulating the DOM of a HTML page. Cheerio used for web-scraping in server-side implementations. Cheerio it is a implementation of jQuery core.  

3. I will use CheerioCrawler when I need parallel crawling of web pages. But, when website requires JavaScript to display the content we need to use PuppeteerCrawler or PlaywrightCrawler instead, because it loads the pages using full-featured headless Chrome browser.

4. We have two main classes for managing requests: RequestQueue, RequestList.
RequestQueue presents a dynamic queue of Requests. One that can be updated at runtime by adding more pages - Requests to process. This allows the crawler to open one page, extract interesting URLs, such as links to other pages on the same domain, add them to the queue (called enqueuing) and repeat this process to build a queue of tens of thousands or more URLs while knowing only a single one at the beginning.

RequestList it is a static, immutable list of URLs and other metadata (see the Request object) that the crawler will visit, one by one, retrying whenever an error occurs, until there are no more Requests to process.

5. We can extract data from a web-page with using library Cheerio (with JQuery syntax, but its not JQuery)

6. SDK uses AutoscaledPool. The auto-scaled pool is started by calling the AutoscaledPool.run() function. The pool periodically queries the AutoscaledPoolOptions.isTaskReadyFunction() function for more tasks, managing optimal concurrency, until the function resolves to false. The pool then queries the AutoscaledPoolOptions.isFinishedFunction(). If it resolves to true, the run finishes after all running tasks complete. If it resolves to false, it assumes there will be more tasks available later and keeps periodically querying for tasks. If any of the tasks throws then the AutoscaledPool.run() function rejects the promise with an error.