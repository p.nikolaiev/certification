const Apify = require('apify');

Apify.main(async () => {
    const asins = []
    const input = await Apify.getValue('INPUT')

    const requestQueue = await Apify.openRequestQueue();
    await requestQueue.addRequest({ url: `https://www.amazon.com/s/?field-keywords=${input.keyword}` });

    const crawler = new Apify.CheerioCrawler({
        requestQueue,
        handlePageFunction: async ({ $, request }) => {
            $('.s-main-slot.s-result-list.s-search-results.sg-row div').each((i, el) => {
                const text = $(el).attr('data-asin')
                if (text != undefined && text != '') {
                    asins.push(text)
                }
            });
        },
    });

    await crawler.run();

    const detailsProductPages = asins.map(asin => ({
        url: `https://www.amazon.com/dp/${asin}`,
        userData: {
            label: 'ASIN',
        },
    }));

    const requestList = await Apify.openRequestList('asins', detailsProductPages);

    const detailCrawler = new Apify.CheerioCrawler({
        requestList,
        handlePageFunction: async ({$, request}) => {
            console.log(`Processing ${request.url}`);

            const results = {
                title: $('#titleSection #productTitle').text().trim(),
                url: request.url,
                description: $('.a-unordered-list.a-vertical.a-spacing-mini li .a-list-item').text().trim().replace(/\n/g,''),
                keyword: input.keyword,
                sellerName: $('#tabular-buybox .tabular-buybox-text').text().trim(),
                price: $('.a-box-inner #price_inside_buybox').text().trim() || $('.a-lineitem #priceblock_ourprice'),
                shippingPrice: $('.a-box-inner .a-size-base.a-color-secondary').text().trim().replace(/\n/g,'') || $('.a-size-base.a-color-secondary').text().trim().replace(/\n/g,'')
            } 
            await Apify.pushData(results)
        }
    }) 
    await detailCrawler.run();
});