Where and how can you use JQuery with the SDK?
What is the main difference between Cheerio and JQuery?
When would you use CheerioCrawler and what are its limitations?
What are the main classes for managing requests and when and why would you use one instead of another?
How can you extract data from a page in Puppeteer without using JQuery?
What is the default concurrency/parallelism the SDK uses?

1. We can use JQuery with Apify SDK for manipulating the DOM of a HTML page.

2. JQuery used for manipulating the DOM of a HTML page. Cheerio used for web-scraping in server-side implementations. Cheerio it is a implementation of jQuery core.  

3. I will use CheerioCrawler when I need parallel crawling of web pages. But, when website requires JavaScript to display the content we need to use PuppeteerCrawler or PlaywrightCrawler instead, because it loads the pages using full-featured headless Chrome browser.

4. We have two main classes for managing requests: RequestQueue, RequestList.
RequestQueue presents a dynamic queue of Requests. One that can be updated at runtime by adding more pages - Requests to process. This allows the crawler to open one page, extract interesting URLs, such as links to other pages on the same domain, add them to the queue (called enqueuing) and repeat this process to build a queue of tens of thousands or more URLs while knowing only a single one at the beginning.

RequestList it is a static, immutable list of URLs and other metadata (see the Request object) that the crawler will visit, one by one, retrying whenever an error occurs, until there are no more Requests to process.

5. We can extract data from a web-page with using library Cheerio (with JQuery syntax, but its not JQuery)

6. SDK uses AutoscaledPool. The auto-scaled pool is started by calling the AutoscaledPool.run() function. The pool periodically queries the AutoscaledPoolOptions.isTaskReadyFunction() function for more tasks, managing optimal concurrency, until the function resolves to false. The pool then queries the AutoscaledPoolOptions.isFinishedFunction(). If it resolves to true, the run finishes after all running tasks complete. If it resolves to false, it assumes there will be more tasks available later and keeps periodically querying for tasks. If any of the tasks throws then the AutoscaledPool.run() function rejects the promise with an error.

How do you allocate more CPU for your actor run?
How can you get the exact time when the actor was started from within the running actor process?
Which are the default storages an actor run is allocated (connected to)?
Can you change the memory allocated to a running actor?
How can you run an actor with Puppeteer in a headful (non-headless) mode?
Imagine the server/instance the container is running on has a 32 GB, 8-core CPU. What would be the most performant (speed/cost) memory allocation for CheerioCrawler? (Hint: NodeJS processes cannot use user-created threads)
(Bonus - Docker)
What is the difference between RUN and CMD Dockerfile commands?
Does your Dockerfile need to contain a CMD command (assuming we don't want to use ENTRYPOINT which is similar)? If yes or no, why?
How does the FROM command work and which base images Apify provides?

1. To allocate more CPU we must allocate more RAM in settings of actor (4GB = 1 CPU, 8GB = 2 CPU)
2. We can see each actor run's exact CU (compute unit) usage under the Info tab in the run's details. Actor with 1GB of allocated   memory for 1 hour, it will consume 1 CU (1 GB memory x 1 hour = 1 CU.)
3. Each actor have 3 default storages: Datasets, Key-value store, Request queue
4. Each actor uses the memory that was allocated before the starting of actor
5. To launch Puppeteer in a headful (non-headless) mode we can add the option headless: false. 
Example: const browser = await puppeteer.launch({headless: false});
6. Cheerio crawlers cannot use more than 1 CPU core, so using more than 4GB memory and more than 1 CPU core not make sense. 
But if we use external binaries such as the Chrome browser, Puppeteer, or other multi-threaded libraries we can use more memory and CPU cores
7. RUN lets execute commands inside of your Docker image
CMD lets define a default command to run when your container starts.
8. We must specify at least one instruction (ENTRYPOINT or CMD) to run docker file. Otherwise we get an error
9. The FROM instruction initializes a new build stage and sets the Base Image for subsequent instructions. 
  Base Docker images:
  - Node.js 14 on Alpine Linux (apify/actor-node)
  - Node.js 14 + Puppeteer + Chrome on Debian (apify/actor-node-puppeteer-chrome)
  - Node.js 14 + Playwright + Chrome on Debian (apify/actor-node-playwright-chrome)