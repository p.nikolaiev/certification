// // const Apify = require('apify')
// // const {utils: {log}} = Apify

// // Apify.main(async () => {
// //     const input = await Apify.getValue('INPUT')
// //     log.info('Starting actor')
// //     const firstUrl = `https://www.amazon.com/s/?field-keywords=${input.keyword}`
// //     const requestList = await Apify.openRequestList('first-url', [{
// //         url: firstUrl,
// //         userData: {label: 'SEARCH'}
// //     }])


// // const requestQueue = await Apify.openRequestQueue()

// // const crawler = new Apify.PuppeteerCrawler({
// //     requestList,
// //         requestQueue,
// //         useSessionPool: true,
// //         persistCookiesPerSession: true,
// //         sessionPoolOptions: {
// //             maxPoolSize: 100,
// //             sessionOptions: {
// //                 maxUsageCount: 5,
// //             },
// //         },
// //         maxRequestsPerCrawl: 50,
// //         launchContext: {
// //             // Chrome with stealth should work for most websites.
// //             useChrome: false,
// //             stealth: true,
            
// //         },


// //     handlePageFunction: async (context) => {
// //         const {url, userData: {label}, } = context.request
// //         const request = context.request
// //         const page = context.page
        
// //         switch(request.userData.label) {
// //             case 'SEARCH':
// //                 const asins = await page.$$eval('.s-asin', (els) => {
// //                     return els.map((el) => el.getAttribute('data-asin'));
// //                 });
            
// //                 for (const ASIN of asins) {
// //                     await requestQueue.addRequest({
// //                         url: `https://www.amazon.com/dp/${ASIN}`,
// //                         userData: { label: 'PRODUCT', ASIN, keyword },
// //                     });
// //                 }
            
// //             case 'PRODUCT':
// //                 const pageTitle = page.$eval('#title', (el) => el.textContent.trim())
// //                 const pageDesription = page.$$eval('#feature-bullets li:not([data-doesntfitmessage])',
// //                 ((items) => items.map((el) => el.textContent?.trim())))
        
// //             case 'OFFER':
// //                 let userData = request.userData

// //                 let offers = await page.$$eval('.aod-offer-heading', elems => {
// //                     return elems.map((elem) => {
// //                         const seller = elem.querySelector('.a-size-small.a-color-base')
// //                         const price = elem.querySelector('.a-price .a-offscreen')
// //                         const shippingPrice = elem.querySelector('a-color-secondary.a-size-base')
// //                         if (!shippingPrice) shippingPrice = 'Not available'
// //                         return {
// //                             seller,
// //                             price,
// //                             shippingPrice
// //                         }
// //                     })
// //                 })

// //                 for(let el in offers) {
// //                     let result = {
// //                         title: userData.title,
// //                         url: userData.url,
// //                         description: userData.description,
// //                         keyword,
// //                     }
// //                     Object.assign(result, el);
// //                     log.info("Pushing result: ", { result })
// //                     await Apify.pushData(result);

// //                 }
// //             }   
// //         }
    
// //     })

// //     log.info('Starting the crawl.');
// //     await crawler.run();
// //     log.info('Crawl finished.');
// // })


// // const Apify = require('apify');

// // Apify.main(async () => {
// //     const asins = []
// //     const input = await Apify.getValue('INPUT')

// //     const requestQueue = await Apify.openRequestQueue();
// //     await requestQueue.addRequest({ url: `https://www.amazon.com/s/?field-keywords=${input.keyword}`, userData: {label: 'SEARCH'} });

// //     const crawler = new Apify.PuppeteerCrawler({
// //         requestQueue,
// //         requestQueue,
// //         handlePageFunction: async ({ $, request, page }) => {
// //             switch(label) {
// //                 case 'SEARCH'
// //             }
// //             asins = await page.$$eval('.s-asin', (products) => {
// //                 return products.map((el) => el.getAttribute('data-asin'));
// //             }); 
            
//             for (const ASIN of asins) {
//                 await requestQueue.addRequest({
//                     url: `https://www.amazon.com/dp/${ASIN}`,
//                     userData: { label: 'PRODUCT', ASIN, keyword },
//                 });
//             }
        
// //         },
// //     });

// //     await crawler.run();

// //     const detailsProductPages = asins.map(asin => ({
// //         url: `https://www.amazon.com/dp/${asin}`,
// //         userData: {
// //             label: 'ASIN',
// //         },
// //     }));

// //     const requestList = await Apify.openRequestList('asins', detailsProductPages);

// //     const detailCrawler = new Apify.PuppeteerCrawler({
// //         requestList,
// //         handlePageFunction: async ({$, request, page}) => {
// //             console.log(`Processing ${request.url}`);

// //             const results = {
// //                 title: documentQuerySelector('#titleSection #productTitle').text().trim(),
// //                 url: request.url,
// //                 description: page.$('.a-unordered-list.a-vertical.a-spacing-mini li .a-list-item').text().trim().replace(/\n/g,''),
// //                 keyword: input.keyword,
// //                 sellerName: page.$('#tabular-buybox .tabular-buybox-text').text().trim(),
// //                 price: page.$('.a-box-inner #price_inside_buybox').text().trim() || $('.a-lineitem #priceblock_ourprice'),
// //                 shippingPrice: page.$('.a-box-inner .a-size-base.a-color-secondary').text().trim().replace(/\n/g,'') || $('.a-size-base.a-color-secondary').text().trim().replace(/\n/g,'')
// //             } 
// //             await Apify.pushData(results)
// //         }
// //     }) 
// //     await detailCrawler.run();
// // });


const Apify = require('apify')

Apify.main( async () => {
    const results = []
    const asins = []
    const input = await Apify.getValue('INPUT')

    const requestQueue = Apify.openRequestQueue()

    const requestList = await Apify.openRequestList('start-url', [{
        url: `https://www.amazon.com/s/?field-keywords=${input.keyword}`,
        userData: { label: "START" },
    }]);


    const handlePageFunction = async ({$, request}) => {
        switch(request.userData.label) {
            case 'START':
                $('.s-main-slot.s-result-list.s-search-results.sg-row div').each((i, el) => {
                    const text = $(el).attr('data-asin')
                    if (text != undefined && text != '') {
                        asins.push(text)
                    }
                });
                for (const ASIN of asins) {
                    await requestQueue.addRequest({
                        url: `https://www.amazon.com/dp/${ASIN}`,
                        userData: { label: 'PRODUCT', ASIN, keyword },
                    });
                }
            case 'PRODUCT':
                console.log(`Processing product ${request.url}`);

                const productDetails = {
                    title: $('#titleSection #productTitle').text().trim(),
                    url: request.url,
                    description: $('.a-unordered-list.a-vertical.a-spacing-mini li .a-list-item').text().trim().replace(/\n/g,''),
                } 
                results.push(productDetails)
                
                for (const ASIN of asins) {
                    await requestQueue.addRequest({
                        url: `https://www.amazon.com/gp/offer-listing/${ASIN}`,
                        userData: { label: 'OFFERS', ASIN, keyword },
                    });
                }
            case 'OFFERS':
                console.log(`Processing offers of product ${request.url}`);

                const pinnedOffer = {
                    pinnedPrice: $('#pinned-offer-top-id .a-offscreen'),
                    pinnedSellerName: $('.a-fixed-left-grid a'),
                    pinnedShippingPrice: $('.a-color-secondary.a-size-base')
                }

                const additionalOffers = await $('#aod-offer', (offers) => {
                    return offers.map((offer) => {
                        const sellerName = offer.querySelector('#aod-offer-soldBy [aria-label]').textContent?.trim()
                        const price = offer.querySelector('.a-offscreen').textContent?.trim();
                        const shippingPrice = offer.querySelector('#delivery-message').textContent?.trim();
                        return {
                            sellerName,
                            price,
                            shippingPrice,
                        };
                    })
                })
                
                
                // let result = {
                //     price:
                //     sellerName:
                //     shippingPrice
                // }
        }   
    }
})