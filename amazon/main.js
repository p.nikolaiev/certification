const Apify = require('apify');

const { utils: { log } } = Apify;

Apify.main(async () => {
    const delay = ms => new Promise(res => setTimeout(res, ms));
    const { keyword } = await Apify.getInput();
    log.info("Starting crawler with keyword: " + keyword);
    const startUrl = `https://www.amazon.com/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords=${keyword}`
    const requestList = await Apify.openRequestList('start-url', [{
        url: startUrl,
        userData: { label: "START" },
    }]);


    const requestQueue = await Apify.openRequestQueue();

    const crawler = new Apify.PuppeteerCrawler({
        requestList,
        requestQueue,
        useSessionPool: true,
        persistCookiesPerSession: true,
        handlePageFunction: async (context) => {
            const { url, userData: { label }, session } = context.request;
            const request = context.request;
            log.info('Page opened.', { label, url });
            const page = context.page;
            const baseURl = 'https://www.amazon.com';

            // START: fetch all ASIN of the products found
            if (request.userData.label === "START") {

                // Get all the products ASIN
                // const ASINList = $('div[data-asin]').map((i, elem) => $(elem).attr('data-asin')).get();
                let ASINList = await page.$$eval('div[data-asin]', (els) => els.map((el) => el.getAttribute('data-asin')));
                log.info('Found ASIN:' + ASINList);
                for (const ASIN of ASINList) {

                    // Filter blank ASINs
                    if (ASIN) {
                        let url = new URL(`https://www.amazon.com/dp/${ASIN}`, baseURl)
                        log.info('Enqueuing product URL:', { href: url.href });
                        await requestQueue.addRequest(
                            {
                                url: url.href,
                                userData: {
                                    label: "DETAIL",
                                    ASIN: ASIN,
                                },
                            });
                    }
                }
            }

            // DETAIL: finding general product info + offers URL
            else if (request.userData.label === "DETAIL") {

                // Find title, url and description of the product
                const title = await page.$eval(
                    'title',
                    (el => el.textContent)
                );

                // Sometimes, the page doesn't contain any description
                const description = await page.$eval(
                    'meta[name^=description]',
                    (el => el != null ? el.getAttribute('content') : '')
                );

                log.info(`Title: ${title}, Description: ${description}`)

                const ASIN = request.userData.ASIN;

                // Query offers
                let offersUrl = new URL(`https://www.amazon.com/gp/offer-listing/${ASIN}`);
                await requestQueue.addRequest(
                    {
                        url: offersUrl.href,
                        userData: {
                            label: 'OFFERS',
                            data: {
                                url: request.url,
                                title: title,
                                description: description
                            },
                        }            
                    });

            }

            // OFFERS: Get the product's offers data
            else if (request.userData.label === "OFFERS") {
                const { request, page} = context;

                log.info('Getting data from offers page');
                let formatedData;
                await delay(10000);
                log.info(request.url);
                if (await page.$('#aod-offer')) {
                    const data = await page.$$eval('#aod-offer', (element) => {
                        const scrapedData = [];
                        element.forEach((el) => {
                            const price = el.querySelector('div#aod-offer span.a-price > .a-offscreen').textContent.trim();
                            const sellerName = el.querySelector('.a-col-right > a.a-link-normal').textContent.trim();
                            const shippingPrice = el.querySelector('.a-size-base.a-color-base .a-color-secondary.a-size-base')
                            
                            scrapedData.push({
                                price,
                                sellerName,
                                shippingPrice,

                            });
                        });
                        return scrapedData;
                    });
                    if (data.length > 0) {
                        formatedData = data.map((el) => {
                            return { ...request.userData.data, ...el };
                        });
                        await Apify.pushData('https://api.apify.com/v2/datasets/9VxKayRSyUdbY1OID?token=vP8ZTgjkYt6YiDQzvDrPngkXS', formatedData);
                    } else {
                        await Apify.pushData({ ...request.userData.data, orderInfo: 'not exist' });
                    }
                    console.log(data);
                }
            }    
        },
    });

    log.info('Starting the crawl.');
    await crawler.run();
    const dataset = await Apify.openDataset();

    // await Apify.call('apify/send-mail', {
    //     to: 'lukas@apify.com',
    //     subject: 'Pavlo Nikolaiev. This is for the Apify Tutorials',
    //     text: `The link to the dataset:
    // https://api.apify.com/v2/datasets/9VxKayRSyUdbY1OID/items`,
    // });

});
